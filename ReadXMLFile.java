import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.*;
import org.w3c.dom.Element;
import org.xml.sax.EntityResolver;
import java.io.File;
import java.util.*;
import javax.xml.parsers.*;

public class ReadXMLFile {

  public static void main(String argv[]) {

    try {
   
	


	String locFile = "D:/hackerrank/SAP/invoice.xml";


	File fXmlFile = new File(locFile);
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	 dBuilder.setEntityResolver(new EntityResolver() {
        @Override
        public InputSource resolveEntity(String publicId, String systemId)
                throws SAXException, IOException {
                	System.out.println(systemId);
                	System.out.println(publicId);
            if (systemId.contains("foo.dtd")) {
                return new InputSource(new StringReader(""));
            } else {
                return null;
            }
        }
    });
	Document doc = dBuilder.parse(fXmlFile);

	//optional, but recommended
	//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
	doc.getDocumentElement().normalize();

	System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

		NodeList nList = doc.getElementsByTagName("PostalAddress");
			System.out.println("_________________________________________");
			for(int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				System.out.println("\nCurrent Element :" + nNode.getNodeName());
				if(nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					NodeList nStreet = eElement.getElementsByTagName("Street");
					Element line = null;
					List<String> streetsList = new ArrayList<String>();
						if (nStreet != null) {

							System.out.println("Strets not null" + nStreet);
					String streets = eElement.getElementsByTagName("Street").item(0).getTextContent();
					//List<String> streetsList = Arrays.asList(streets.split(" "));
					
					List<String> streetTemp= new ArrayList<String>();
					for (int j= 0; j < nStreet.getLength(); j++) {
							line = (Element)nStreet.item(j);
							if(line != null) {
								streets = line.getTextContent();
								if(streets != ""){
								streetTemp = Arrays.asList(streets.split(" "));
								streetsList.addAll(streetTemp);
								System.out.println("Test: "+ line.getTextContent());
							}
								//streetsList.add(getCharacterDataFromElement(line));
							}
							
						}

					}
						
					System.out.println(" Street  combined: "+ streetsList );
					String city = eElement.getElementsByTagName("City").item(0).getTextContent();
					System.out.println("City : "+ city);
					String state = eElement.getElementsByTagName("State").item(0).getTextContent();
					System.out.println("State : " + state);
					String postalCode = eElement.getElementsByTagName("PostalCode").item(0).getTextContent();
					System.out.println("Postal Code : " + postalCode);
					String country = eElement.getElementsByTagName("Country").item(0).getTextContent();
					System.out.println("Country Code : " + country);


				}
			}
    } catch (Exception e) {
	e.printStackTrace();
    }
  }

}